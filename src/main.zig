const std = @import("std");
const c = @cImport({
    @cInclude("raylib.h");
});

const EntityKind = struct { color: c.Color, speed: u8, collisionSpeed: u8, collisionDirection: f16 };

const Entity = struct {
    x: f16,
    y: f16,
    direction: f16,
    kind: EntityKind,
};

pub fn main() !void {
    const screenWidth = 1600;
    const screenHeight = 900;

    const numberOfKinds = 10;
    const numberOfEntities = 100;

    const time: u128 = @bitCast(std.time.nanoTimestamp());
    const seed: u64 = @truncate(time);
    var rnd = std.rand.DefaultPrng.init(seed);

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();
    var kinds = std.ArrayList(EntityKind).init(allocator);
    var entities = std.ArrayList(Entity).init(allocator);

    for (0..numberOfKinds) |i| {
        _ = i;
        try kinds.append(EntityKind{
            .color = c.ColorFromHSV(rnd.random().float(f32) * 360, 0.7, 0.6),
            .speed = rnd.random().int(u8) % 10,
            .collisionSpeed = rnd.random().int(u8) % 20,
            .collisionDirection = @floatCast(rnd.random().float(f32) * 360),
        });
    }

    for (0..numberOfEntities) |i| {
        _ = i;
        try entities.append(Entity{ .x = 800, .y = 450, .direction = @floatCast(rnd.random().float(f32) * 2 * std.math.pi), .kind = kinds.items[rnd.random().int(usize) % numberOfKinds] });
        std.debug.print("{d}|{d}|{d},\t{d},\t{d},\t{d},\t{d}\n", .{
            entities.getLast().kind.color.r,
            entities.getLast().kind.color.g,
            entities.getLast().kind.color.b,
            entities.getLast().kind.speed,
            @cos(entities.getLast().direction) * @as(f16, @floatFromInt(entities.getLast().kind.speed)),
            @sin(entities.getLast().direction) * @as(f16, @floatFromInt(entities.getLast().kind.speed)),
            entities.getLast().direction,
        });
    }

    c.InitWindow(screenWidth, screenHeight, "tzeger");
    defer c.CloseWindow();

    c.SetTargetFPS(60);

    while (!c.WindowShouldClose()) {
        c.BeginDrawing();

        c.ClearBackground(c.BLACK);
        for (entities.items, 0..) |entity, i| {
            entities.items[i].x += @cos(entity.direction) * @as(f16, @floatFromInt(entity.kind.speed));
            entities.items[i].y += @sin(entity.direction) * @as(f16, @floatFromInt(entity.kind.speed));

            entities.items[i].x = @mod(entities.items[i].x, screenWidth);
            entities.items[i].y = @mod(entities.items[i].y, screenHeight);

            if (entities.items[i].x < 0) {
                entities.items[i].x += screenWidth;
            }
            if (entities.items[i].y < 0) {
                entities.items[i].y += screenHeight;
            }
            c.DrawCircle(@as(c_int, @intFromFloat(entity.x)), @as(c_int, @intFromFloat(entity.y)), 3.0, entity.kind.color);
        }

        c.EndDrawing();
    }
}
